//Estructura del Framework

+ Subdividimos el framework en los siguiente bloques:

	1) lib -> Archivos globales, de configuración y genéricos.
	2) modules -> Archivos específicos por elementos (tipografías, botones, formularios, etc..)
	3) plugins -> Añadimos css de terceros, que necesitemos en nuestros proyectos
	4) styleguide -> Incorpora los estilos específicos de la guía de estilos del framework

+ Orden de carga:

	1) lib/_variables.scss -> El resto de scss necesitan estos valores
	2) lib/_mixins.scss -> Funciones que utilizaremos
	3) lib/_reset.scss -> Reseteamos los estilos
	4) modules/? -> Los elementos que necesitemos de modulos, yendo de lo más genérico a lo más concreto, para que no haya conflictos de herencia
	5) plugins/? -> Los css externos. No deberían tener conflicto con los propios.
	6) lib/_shame.scss -> Sitio en donde meter los hacks que necesitemos meter por prisas durante la programación. Así están reunidos, y pueden hacerse bien cuando sea posible.