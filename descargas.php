<?php
$texto = "Como ya hemos visto antes, la caja madre no se amplía normalmente para contener a los hijos flotantes. Esto a menudo puede provocar confusiones, por ejemplo cuando todos los hijos de un elemento flotan al crear un menú horizontal a partir de una lista no ordenada mediante la flotación de todos los elementos li. Como las cajas flotantes se sacan del flujo y no afectan a la caja madre, el hecho de hacer flotar las hijas hace que en realidad la madre quede vacía y que pase a tener una altura cero. Algunas veces esto no es lo que queremos, por ejemplo a la hora de definir un fondo para la madre. Si la madre tiene una altura cero, el fondo no se verá.\n\r\n\rEs evidente que necesitamos algún mecanismo para conseguir que una caja madre se amplíe para incluir a sus hijas flotantes. El método tradicional consistía en incluir un elemento adicional en el etiquetado, justo antes de la etiqueta de cierre de la caja madre, y definir en él clear:both. Esto funciona, pero no es muy aceptable porque implica introducir un etiquetado adicional poco semántico e innecesario. Por suerte, hay otras maneras que explicaremos a continuación.";

if(!empty($_GET["download"])){
	if($_GET["download"] == "content"){
		$filename = 'nombre_archivo.txt';
			
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Length: ". filesize("$filename").";");
		header("Content-Disposition: attachment; filename=$filename");
		header("Content-Type: application/octet-stream; "); 
		header("Content-Transfer-Encoding: binary");

		echo($texto);
		die;	//Para que no escriba el resto del html
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Menús HTML5 + CSS3</title>
	<style>
	body{font-size:16px;color:#000;background-color:#fff;margin:0;padding:0;font-family: Verdana, Arial, Helvetica;}
	</style>
</head>
<body>
	<header>
		<p>DESCARGA DE ARCHIVOS</p>
	</header>
	<section>
		<article>
			<header>
				<p>+ Ejemplo de como descargar un contenido directamente desde php simplemente con cabeceras mediante un echo y sin necesidad de guardar ni crear el archivo en el servidor.</p>
			</header>
			<a href="?download=content">Pulsa para descargar en un txt el siguiente texto:</a>
			<p><?php echo $texto;?></p>
		</article>
		<article>
			<header>
				<p>+ Ejemplo de como descargar un archivo directamente desde php simplemente con cabeceras.</p>
			</header>
			<a href="?download=file">Pulsa para descargar un archivo del servidor:</a>
		</article>
	</section>
	<aside>
		<h2></h2>
		<p></p>
	</aside>
	<footer>
		<p>&copy; Ignacio Navarro</p>
	</footer>
</body>
</html>