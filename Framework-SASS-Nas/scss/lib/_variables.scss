@charset "UTF-8";
// **************
// * $VARIABLES *
// **************


// $BREAKPOINTS 
// ************
$bp1 : 30em;		//480px
$bp2 : 37.5em;		//600px
$bp3 : 48em;		//768px
$bp4 : 56.25em;		//900px
$bp5 : 68.75em;		//1100px


// $PATHS
// ******
$path-img : "img/";
$path-fonts : "http://local.git.com/ignacio-personal/Framework-SASS-Nas/fonts/";


// $COLORES
// *******

$color-default      	: #00434C;
$color-default-light	: lighten($color-default, 15%);
$color-secondary   		: #B29F09;
$color-secondary-light	: lighten($color-secondary, 50%);
$color-tertiary   		: #721370;
$color-tertiary-light	: lighten($color-tertiary, 50%);

$black 		: #000000;
$grey-dark 	: lighten($black, 25%);
$grey 		: lighten($black, 50%);
$grey-light : lighten($black, 75%);
$white 		: #FFFFFF;

$color-info 	: #336699;
$color-success 	: #19DB02;
$color-warning 	: #E8AA0C;
$color-error 	: #F11B23;
$color-alert 	: $color-info;


// Web
$web-background		: $white;
$web-background-dev	: $color-warning;

// Colores sociales: Facebook, twitter, etc..
$fb :rgb(59, 89, 182);
$tw :rgb(64,153,255);
$googleplus :rgb(221, 75, 57);
$pinterest :rgb(203, 32, 39);
$skype :rgb(18,165,244);
$spotify :rgb(129,183,26);
$instagram:#4E433C;
$tumblr: #2B4964;
$vimeo: #86B32D;
$youtube: #FF3333;
$linkedin: #4875B4;
$flickr: #FE0883;

//Mantenemos aqui una lista actualizada de colores para poder usar en el styleguide dinámicamente
$color-list : 	(color-default : $color-default,color-default-light : $color-default-light,color-secondary : $color-secondary,color-secondary-light : $color-secondary-light,
				color-tertiary : $color-tertiary,color-tertiary-light : $color-tertiary-light,web-background : $web-background,black : $black,grey-dark : $grey-dark,
				grey : $grey,grey-light : $grey-light,white : $white,color-info : $color-info,color-success : $color-success,color-warning : $color-warning,
				color-error : $color-error,color-alert : $color-alert,fb : $fb,tw : $tw,googleplus : $googleplus,pinterest : $pinterest,skype : $skype,spotify : $spotify,
				instagram : $instagram,tumblr : $tumblr,vimeo : $vimeo,youtube : $youtube,linkedin : $linkedin,flickr : $flickr);

// $FONTS
// ******
@import url(http://fonts.googleapis.com/css?family=Raleway:300);
$font-family-default	: 'Raleway', Verdana, Helvetica;
$font-family-fallback	: Arial, Georgia;
$font-family-menu		: $font-family-default;
$font-size-default		: 16;	//Tamaño en pixeles, pero sin poner el px
$font-size-xs			: $font-size-default - ( 40 * $font-size-default / 100);
$font-size-s			: $font-size-default - ( 20 * $font-size-default / 100);
$font-size-l			: $font-size-default + ( 20 * $font-size-default / 100);
$font-size-xl			: $font-size-default + ( 40 * $font-size-default / 100);
$font-color-default		: $grey-dark;


// $LINKS 
//*******
$link-color 		: $font-color-default;
$link-color-hover 	: darken($link-color, 10);
$link-color-visited : $font-color-default;
$link-color-focus 	: darken($link-color-visited, 10);


// $LAYOUT
// *******
$page-width-max 	: 80%;
$page-line-height 	: 1.5em;
$baseline 			: $page-line-height;
$gutterwidth 		: 24px;
$transition-hover 	: .3s;
$animation-time 	: .8s;
$radius 			: 5px;

// $MENUS
// ******
$nav-width			: auto;
$nav-height			: 3rem;
$nav-color			: $white;
$nav-background		: $color-default;
$nav-li-background	: $color-secondary;
$nav-borders		: lighten($nav-li-background, 90%);
$nav-icon-size		: 16px;


// $BOTONES
//*********
$button-background   	: $color-default-light;
$button-border-color	: darken($button-background, 10%);
$button-border-width  	: 1px;
$button-border-style  	: solid;
$button-text-color      : $black;
$button-radius		    : $radius;


// $FORMULARIOS
//*************
$form-input-width		: 100%;
$form-input-height		: 1.5rem;
$form-input-background  : $grey-light;
$form-input-border      : $grey-dark;
$form-input-borderwidth : 1px;
$form-input-borderstyle : solid;
$form-input-hover       : lighten($black, 50%);
$form-input-focus       : $color-default, 40%;
$form-placeholder-color	: $grey-light;


// $TABLAS
//********
$table-background   : transparent;
$table-stripe       : lighten($color-default, 60%);
$table-hover        : lighten($color-default, 40%);
$table-border		: lighten($color-default, 20%);
$table-border-width	: 1px;
$table-border-style	: solid;
$table-radius	    : $radius;


// $AVISOS Y POPUPS
//*****************
$alert-border-width : 1px;
$alert-border-style : solid;

//Colores
$alert-color-success 	: lighten($color-success, 30%);
$alert-color-error	 	: lighten($color-error, 30%);
$alert-color-info		: lighten($color-info, 10%);
$alert-color-warning 	: lighten($color-warning, 30%);


//PENDIENTE DE REVISAR E INCORPORAR

//$no-mediaquery-class
//********************
// Si decides incluir lt-ie9 para dar soporte a navegadores sin estilos esta es la clase que se añadirá a todos los elementos.
$no-mq-classname: ".ie8-sucks";