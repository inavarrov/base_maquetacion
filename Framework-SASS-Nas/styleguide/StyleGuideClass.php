<?php
class StyleGuideClass
{

	
	/**
	* Funcion que devuelve a partir de un nombre de variable de color el html que lo muestra en el StyleGuide
	* @param $variable_color, nombre de la variable de color que quiere mostrarse
	* @return html
	*/
	public static function printColorPattern($variable_color){
		$html = '
				<div class="div-patron-color">
					<figure class="figure-color-wrap">
						<div class="figure-div-color '.$variable_color.'"></div>
						<div class="figcaption-color">
							<span class="hex_'.$variable_color.'"><br/>'.$variable_color.'</span>
						</div>
					</figure>
				</div>';
		return $html;
	}

}
?>