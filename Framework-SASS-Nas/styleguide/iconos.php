<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_apertura.php');
/*Fin de Elementos comunes a todas las páginas*/
?>
<main class="main-styleguide">
	<section>
		<header>
			<h1 class="h1_principal">Guía de estilos base</h1>
			<h2 class="h2_principal">ICONOS</h2>
		</header>
		<article class="article">
			<p>Iconos creados como fuente desde www.icomoon.io, que tenemos descargados directamente en el proyectos y que usamos como tipo font.</header>
		</article>
		<article class="article-iconos">
			<header>icon-spinner</header>
			<span class="icon-spinner"></span>
		</article>
		<article class="article-iconos">
			<header>icon-home</header>
			<span class="icon-home"></span>
		</article>
		<article class="article-iconos">
			<header>icon-file</header>
			<span class="icon-file"></span>
		</article>
		<article class="article-iconos">
			<header>icon-calendar</header>
			<span class="icon-calendar"></span>
		</article>
		<article class="article-iconos">
			<header>icon-lock</header>
			<span class="icon-lock"></span>
		</article>
		<article class="article-iconos">
			<header>icon-unlocked</header>
			<span class="icon-unlocked"></span>
		</article>
		<article class="article-iconos">
			<header>icon-cog</header>
			<span class="icon-cog"></span>
		</article>
		<article class="article-iconos">
			<header>icon-remove</header>
			<span class="icon-remove"></span>
		</article>
		<article class="article-iconos">
			<header>icon-close</header>
			<span class="icon-close"></span>
		</article>
		<article class="article-iconos">
			<header>icon-checkmark</header>
			<span class="icon-checkmark"></span>
		</article>
		<article class="article-iconos">
			<header>icon-facebook</header>
			<span class="icon-facebook"></span>
		</article>
		<article class="article-iconos">
			<header>icon-twitter</header>
			<span class="icon-twitter"></span>
		</article>
		<article class="article-iconos">
			<header>icon-vimeo</header>
			<span class="icon-vimeo"></span>
		</article>
		<article class="article-iconos">
			<header>icon-tumblr</header>
			<span class="icon-tumblr"></span>
		</article>
	</section>
</main>
<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_cierre.php');
/*Fin de Elementos comunes a todas las páginas*/
?>