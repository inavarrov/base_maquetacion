<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_apertura.php');
/*Fin de Elementos comunes a todas las páginas*/
?>
<main class="main-styleguide">
	<section>
		<header>
			<h1>Guía de estilos base</h1>
			<h2>TIPOGRAFÍA</h2>
		</header>
		<article class="article-tipografia">
			<header class="styleguide">Headers por defecto</header>
			<p>A continuación vemos los distintos formatos de encabezados definidos por defecto</p>
			<h1>Encabezado h1 principal</h1>
			<p><b>Uso: &lt;h1&gt;Título&lt;h1&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<h2>Encabezado h2</h2>
			<p><b>Uso: &lt;h2&gt;Título&lt;h2&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio possimus nesciunt pariatur nostrum, molestias doloribus sint tempora deleniti repellendus autem distinctio ipsum tenetur consequatur ut consequuntur fuga ipsa? Repellat, fugit.</p>
			<h3>Encabezado h3</h3>
			<p><b>Uso: &lt;h3&gt;Título&lt;h3&gt;</b></p>
			<p>Lorem ipsum dolor sit amdt, consectetur adipisicing elit. Doloremque, libero! Non eum fugit, voluptatum ipsa, sequi nam ex nemo, veritatis sit ad incidunt. Nesciunt maxime ratione, voluptas quidem sed sunt.</p>
			<h4>Encabezado h4</h4>
			<p><b>Uso: &lt;h4&gt;Título&lt;h4&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
			<h5>Encabezado h5</h5>
			<p><b>Uso: &lt;h5&gt;Título&lt;h5&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum cupiditate error alias nisi, obcaecati facilis cumque esse ducimus, quaerat aspernatur autem sint hic ratione ipsum, doloremque natus fuga numquam repudiandae?</p>
			<h6>Encabezado h6</h6>
			<p><b>Uso: &lt;h6&gt;Título&lt;h6&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam quos doloribus, maiores quam iusto voluptas sapiente quo placeat tenetur dicta temporibus, asperiores quisquam est, explicabo tempora similique assumenda. Iure, nihil.</p>
		</article>
		<article class="article-tipografia">
			<header class="styleguide">Headers con subrayado</header>
			<p>A continuación vemos los distintos formatos de encabezados con subrayado del mismo color que el texto, que es menos grueso en función de la importancia.</p>
			<h1 class="underline">Encabezado h1 principal</h1>
			<p><b>Uso: &lt;h1 class="undeline"&gt;Titulo&lt;h1&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<h2 class="underline">Encabezado h2</h2>
			<p><b>Uso: &lt;h2 class="undeline"&gt;Titulo&lt;h2&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio possimus nesciunt pariatur nostrum, molestias doloribus sint tempora deleniti repellendus autem distinctio ipsum tenetur consequatur ut consequuntur fuga ipsa? Repellat, fugit.</p>
			<h3 class="underline">Encabezado h3</h3>
			<p><b>Uso: &lt;h3 class="undeline"&gt;Titulo&lt;h3&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, libero! Non eum fugit, voluptatum ipsa, sequi nam ex nemo, veritatis sit ad incidunt. Nesciunt maxime ratione, voluptas quidem sed sunt.</p>
			<h4 class="underline">Encabezado h4</h4>
			<p><b>Uso: &lt;h4 class="undeline"&gt;Titulo&lt;h4&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
			<h5 class="underline">Encabezado h5</h5>
			<p><b>Uso: &lt;h5 class="undeline"&gt;Titulo&lt;h5&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum cupiditate error alias nisi, obcaecati facilis cumque esse ducimus, quaerat aspernatur autem sint hic ratione ipsum, doloremque natus fuga numquam repudiandae?</p>
			<h6 class="underline">Encabezado h6</h6>
			<p><b>Uso: &lt;h6 class="undeline"&gt;Titulo&lt;h6&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam quos doloribus, maiores quam iusto voluptas sapiente quo placeat tenetur dicta temporibus, asperiores quisquam est, explicabo tempora similique assumenda. Iure, nihil.</p>
		</article>
		<article class="article-tipografia">
			<header class="styleguide">Headers con subrayado punteado</header>
			<p>A continuación vemos los distintos formatos de encabezados con subrayado dotted del mismo color en todos los tipos.</p>
			<h1 class="underdotted">Encabezado h1 principal</h1>
			<p><b>Uso: &lt;h1 class="underdotted"&gt;Titulo&lt;h1&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<h2 class="underdotted">Encabezado h2</h2>
			<p><b>Uso: &lt;h2 class="underdotted"&gt;Titulo&lt;h2&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio possimus nesciunt pariatur nostrum, molestias doloribus sint tempora deleniti repellendus autem distinctio ipsum tenetur consequatur ut consequuntur fuga ipsa? Repellat, fugit.</p>
			<h3 class="underdotted">Encabezado h3</h3>
			<p><b>Uso: &lt;h3 class="underdotted"&gt;Titulo&lt;h3&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, libero! Non eum fugit, voluptatum ipsa, sequi nam ex nemo, veritatis sit ad incidunt. Nesciunt maxime ratione, voluptas quidem sed sunt.</p>
			<h4 class="underdotted">Encabezado h4</h4>
			<p><b>Uso: &lt;h4 class="underdotted"&gt;Titulo&lt;h4&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
			<h5 class="underdotted">Encabezado h5</h5>
			<p><b>Uso: &lt;h5 class="underdotted"&gt;Titulo&lt;h5&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum cupiditate error alias nisi, obcaecati facilis cumque esse ducimus, quaerat aspernatur autem sint hic ratione ipsum, doloremque natus fuga numquam repudiandae?</p>
			<h6 class="underdotted">Encabezado h6</h6>
			<p><b>Uso: &lt;h6 class="underdotted"&gt;Titulo&lt;h6&gt;</b></p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam quos doloribus, maiores quam iusto voluptas sapiente quo placeat tenetur dicta temporibus, asperiores quisquam est, explicabo tempora similique assumenda. Iure, nihil.</p>
		</article>
		<article class="article-tipografia">
			<header class="styleguide">Párrafos</header>
			<h4>Párrafos por defecto</h4>
			<p><b>Uso:</b> &lt;p&gt;Contenido del párrafo&lt;/p&gt;</p>
			<p><b>Ejemplo:</b></p>
			<p>Párrafos por defecto en los que vemos el tipo de fuente, tamaño, margen inferior, etc.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
			<h4>Párrafos con márgenes</h4>
			<p><b>Uso:</b> &lt;p class="margindefault"&gt;Contenido del párrafo&lt;/p&gt;</p>
			<p><b>Ejemplo:</b></p>
			<p class="margindefault">Párrafos con el margen por defecto en los que vemos el tipo de fuente, tamaño, margen inferior, etc.</p>
			<p class="margindefault">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<p class="margindefault">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
			<h4>Párrafos con identación</h4>
			<p><b>Uso:</b> &lt;p class="idented"&gt;Contenido del párrafo&lt;/p&gt;</p>
			<p><b>Ejemplo:</b></p>
			<p class="idented">Párrafos con el margen por defecto en los que vemos el tipo de fuente, tamaño, margen inferior, etc.</p>
			<p class="idented">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, molestias, dolore. Libero aliquam enim quod aperiam inventore quae quia a, rerum veritatis, consectetur, quidem modi? Eius nulla blanditiis sint iure.</p>
			<p class="idented">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio odit necessitatibus, deserunt quibusdam aliquam quis qui dolore voluptate eveniet voluptatum, earum neque itaque id harum, eaque, magni tenetur. Aperiam, eum.</p>
		</article>
	</section>
</main>
<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_cierre.php');
/*Fin de Elementos comunes a todas las páginas*/
?>