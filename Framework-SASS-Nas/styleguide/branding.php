<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_apertura.php');
/*Fin de Elementos comunes a todas las páginas*/
?>
<main class="main-styleguide">
	<section>
		<header>
			<h1 class="h1_principal">Guía de estilos base</h1>
			<h2 class="h2_principal">BRANDING</h2>
		</header>
<p style="color:red;">PEDIR A JUANJO QUE ME REHAGA EL LOGO EN VECTORIAL, QUE SOLO TENGO ESTE TAMAÑO</p>
		<article class="article-branding">
			<p>En esta sección queremos guardar la imagen corportativa: logos, elementos..</p>
		</article>
		<article class="article-branding">
			<h3 class="underline">Logo con fondo blanco</h3>
			<p>
				<b>Características:</b>
				<br/>Tamaño: 287x36px
				<br/>Nombre de archivo: logo.gif
			</p>
			<p class="center"><img src="img/logo.gif" alt="Logo" class="branding-img"/></p>
		</article>
		<article class="article-branding">
			<h3 class="underline">Logo con fondo verde</h3>
			<p>
				<b>Características:</b>
				<br/>Tamaño: 287x36px
				<br/>Nombre de archivo: logo_valdecantos.gif
			</p>
			<p class="center"><img src="img/logo_valdecantos.gif" alt="Logo" class="branding-img"/></p>
		</article>
		<article class="article-branding">
			<h3 class="underline">Favicon</h3>
			<p>
				<b>Características:</b>
				<br/>Tamaño: 48x48px
				<br/>Nombre de archivo: logo_favoritos.ico
			</p>
			<p class="center"><img src="img/logo_favoritos.ico" style="width:48px;height:48px;" alt="Icono Favoritos" class="branding-img"/></p>
		</article>
		<article class="article-branding">
			<h3 class="underline">Logo con fondo transparente</h3>
			<p>
				<b>Características:</b>
				<br/>Tamaño: 309x58px
				<br/>Nombre de archivo: logo_valdecantost.gif
			</p>
			<p class="center"><img src="img/logo_valdecantost.gif" alt="Logo" class="branding-img"/></p>
		</article>
		<article class="article-branding">
			<h3 class="underline">Logo sin texto en verde</h3>
			<p>
				<b>Características:</b>
				<br/>Tamaño: 45x43px
				<br/>Nombre de archivo: logo_verde.gif
			</p>
			<p class="center"><img src="img/logo_verde.gif" alt="Logo" class="branding-img"/></p>
		</article>
	</section>
</main>
<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_cierre.php');
/*Fin de Elementos comunes a todas las páginas*/
?>