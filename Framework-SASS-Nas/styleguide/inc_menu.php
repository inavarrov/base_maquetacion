<?php 
//Vemos en que pagina estamos
$page = substr(basename($_SERVER['PHP_SELF']), 0, -4);
?>
<nav class="styleguide">
	<input type="checkbox" id="trigger_menu"/>
	<label for="trigger_menu" class="label_trigger_menu"> </label>
	<ul>
		<li <?php if($page == "index") echo('class="selected"');?>><a href="index" alt="">Colores</a></li>
		<li <?php if($page == "variables") echo('class="selected"');?>><a href="variables" alt="">Variables</a></li>
		<li <?php if($page == "branding") echo('class="selected"');?>><a href="branding" alt="">Branding</a></li>
		<li <?php if($page == "tipografia") echo('class="selected"');?>><a href="tipografia" alt="">Tipografía</a></li>
		<li <?php if($page == "formularios") echo('class="selected"');?>><a href="formularios" alt="">Formularios</a></li>
		<li <?php if($page == "tablas") echo('class="selected"');?>><a href="tablas" alt="">Tablas</a></li>
		<li <?php if($page == "listas") echo('class="selected"');?>><a href="listas" alt="">Listas</a></li>
		<li <?php if($page == "botones") echo('class="selected"');?>><a href="botones" alt="">Botones</a></li>
		<li <?php if($page == "imagenes") echo('class="selected"');?>><a href="imagenes" alt="">Imágenes</a></li>
		<li <?php if($page == "iconos") echo('class="selected"');?>><a href="iconos" alt="">Iconos</a></li>
	</ul>
</nav>