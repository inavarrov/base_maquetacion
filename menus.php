<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Menús HTML5 + CSS3</title>
	<style>
	body{font-size:16px;color:#000;background-color:#fff;margin:0;padding:0;font-family: Verdana, Arial, Helvetica;}
	/*Horizontal*/
	ul.horizontal{
	    padding:0;
	    margin:0;
	    list-style-type: none;
	    display:table;
	    width:100%;
	}
	ul.horizontal li{ 
	    display: table-cell;
	    height: 35px;
	    line-height:35px;
	    font-size: 16px;
	    color:#fff;
	    background-color: #65A2DC;
	    text-decoration: none;
	    border-right:1px solid #fff;
	    text-align: center;
	}
	ul.horizontal li:last-child{ 
		border-right: none;
	}
	ul.horizontal li:hover{
		cursor:pointer;
		background-color: #369;
	} 
	ul.horizontal li a{
		color:#fff;
	    text-decoration: none;
	}
	ul.horizontal li.selected{ 
	    color:#000;
	    background-color: #369;
	}	
	/*Horizontal con borde redondeado*/
	ul.horizontal_redondeado{
	    padding:0;
	    margin:0;
	    list-style-type: none;
	    display:table;
	    width:100%;
	}
	ul.horizontal_redondeado li{ 
	    display: table-cell;
	    height: 35px;
	    line-height:35px;
	    font-size: 16px;
	    color:#fff;
	    background-color: #65A2DC;
	    text-decoration: none;
	    border-right:1px solid #fff;
	    text-align: center;
	    -webkit-border-top-left-radius: 20px;
	    -webkit-border-top-right-radius: 20px;
	    -moz-border-radius-topleft: 20px;
	    -moz-border-radius-topright: 20px;
	    border-top-left-radius: 20px;
	    border-top-right-radius: 20px;
	}
	ul.horizontal_redondeado li:last-child{ 
		border-right: none;
	}
	ul.horizontal_redondeado li:hover{
		cursor:pointer;
		background-color: #369;
	} 
	ul.horizontal_redondeado li a{
		color:#fff;
	    text-decoration: none;
	}
	ul.horizontal_redondeado li.selected{ 
	    color:#000;
	    background-color: #369;
	}	
	</style>
</head>
<body>
	<header>
		<p>CONTENIDOS DE HTML5+CSS3</p>
		<nav>
			<ul>
				<li><a href="#horizontal">Menú Horizontal</a></li>
				<li><a href="#horizontal">Menú Horizontal con bordes redondeados</a></li>
			</ul>
		</nav>
	</header>
	<section>
		<article>
			<header>
				<h2 name="horizontal">Menú horizontal</h2>
				<p>En este menú si muestran los elementos en una sola fila y sin los marcadores por defecto</p>
			</header>
			<ul class="horizontal">
				<li><a href="#horizontal">Opción 1</a></li>
				<li><a href="#horizontal">Opción 2</a></li>
				<li><a href="#horizontal">Opción 3</a></li>
				<li><a href="#horizontal">Opción 4</a></li>
				<li class="selected">Opción 5</li>
				<li><a href="#horizontal">Opción 6</a></li>
			</ul>
		</article>
		<article>
			<header>
				<h2 name="horizontal_redondeado">Menú horizontal con bordes redondeados</h2>
				<p>En este menú si muestran los elementos en una sola fila y sin los marcadores por defecto</p>
			</header>
			<ul class="horizontal_redondeado">
				<li><a href="#horizontal_redondeado">Opción 1</a></li>
				<li><a href="#horizontal_redondeado">Opción 2</a></li>
				<li><a href="#horizontal_redondeado">Opción 3</a></li>
				<li><a href="#horizontal_redondeado">Opción 4</a></li>
				<li class="selected">Opción 5</li>
				<li><a href="#horizontal_redondeado">Opción 6</a></li>
			</ul>
		</article>
	</section>
	<aside>
		<h2></h2>
		<p></p>
	</aside>
	<footer>
		<p>&copy; Ignacio Navarro</p>
	</footer>
</body>
</html>