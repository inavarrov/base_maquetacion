<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_apertura.php');
/*Fin de Elementos comunes a todas las páginas*/
?>
<main class="main-styleguide">
	<section>
		<header>
			<h1 class="h1_principal">Guía de estilos base</h1>
			<h2 class="h2_principal">COLORES</h2>
		</header>
		<article class="article-colores">
			<header>Colores base</header>
			<?php
			echo(StyleGuideClass::printColorPattern('color-default'));
			echo(StyleGuideClass::printColorPattern('color-default-light'));
			echo(StyleGuideClass::printColorPattern('color-secondary'));
			echo(StyleGuideClass::printColorPattern('color-secondary-light'));
			echo(StyleGuideClass::printColorPattern('color-tertiary'));
			echo(StyleGuideClass::printColorPattern('color-tertiary-light'));
			echo(StyleGuideClass::printColorPattern('web-background'));
			?>
		</article>
		<article class="article-colores">
			<header>Escala de grises</header>
			<?php
			echo(StyleGuideClass::printColorPattern('black'));
			echo(StyleGuideClass::printColorPattern('grey-dark'));
			echo(StyleGuideClass::printColorPattern('grey'));
			echo(StyleGuideClass::printColorPattern('grey-light'));
			echo(StyleGuideClass::printColorPattern('white'));
			?>
		</article>
		<article class="article-colores">
			<header>Popups y avisos</header>
			<?php
			echo(StyleGuideClass::printColorPattern('color-info'));
			echo(StyleGuideClass::printColorPattern('color-success'));
			echo(StyleGuideClass::printColorPattern('color-warning'));
			echo(StyleGuideClass::printColorPattern('color-error'));
			echo(StyleGuideClass::printColorPattern('color-alert'));
			?>
		</article>
		<article class="article-colores">
			<header>Colores sociales</header>
			<?php
			echo(StyleGuideClass::printColorPattern('fb'));
			echo(StyleGuideClass::printColorPattern('tw'));
			echo(StyleGuideClass::printColorPattern('googleplus'));
			echo(StyleGuideClass::printColorPattern('pinterest'));
			echo(StyleGuideClass::printColorPattern('skype'));
			echo(StyleGuideClass::printColorPattern('spotify'));
			echo(StyleGuideClass::printColorPattern('instagram'));
			echo(StyleGuideClass::printColorPattern('tumblr'));
			echo(StyleGuideClass::printColorPattern('vimeo'));
			echo(StyleGuideClass::printColorPattern('youtube'));
			echo(StyleGuideClass::printColorPattern('linkedin'));
			echo(StyleGuideClass::printColorPattern('flickr'));
			?>
		</article>
	</section>
</main>
<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_cierre.php');
/*Fin de Elementos comunes a todas las páginas*/
?>