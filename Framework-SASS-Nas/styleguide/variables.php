<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_apertura.php');
/*Fin de Elementos comunes a todas las páginas*/
?>
<main class="main-styleguide">
	<section>
		<header>
			<h1 class="h1_principal">Guía de estilos base</h1>
			<h2 class="h2_principal">VARIABLES</h2>
		</header>
		<article class="article-variables">
			<p>Tenemos aquí una relación de las variables creadas en el framework, de modo que sea un punto de referencia sencillo en caso de que se quieran consultar nombres.</p>
		</article>
		<article class="article-variables">
			<?php echo nl2br(file_get_contents("../scss/lib/_variables.scss"));?>
		</article>
	</section>
</main>
<?php
/*Elementos comunes a todas las páginas*/
include_once('inc_comun_cierre.php');
/*Fin de Elementos comunes a todas las páginas*/
?>